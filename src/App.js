import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const reloadClassName = reloading => (reloading ? 'reloading' : '');

const reloadErrorClassName = (reloading, error) => {
  if (error) {
    return 'error';
  } else if (reloading) {
    return 'reloading';
  }
  return '';
};

const apiURL = 'http://qmenta-devtest.herokuapp.com';

const allDicoms = [
'T1W_SE_1/IM-0004-0001.dcm', 'T1W_SE_2/IM-0005-0002.dcm', 'T1W_SE_2/IM-0005-0016.dcm', 'T1W_SE_2/IM-0005-0030.dcm',
'T1W_SE_1/IM-0004-0002.dcm', 'T1W_SE_2/IM-0005-0003.dcm', 'T1W_SE_2/IM-0005-0017.dcm', 'T1W_SE_2/IM-0005-0031.dcm',
'T1W_SE_1/IM-0004-0003.dcm', 'T1W_SE_2/IM-0005-0004.dcm', 'T1W_SE_2/IM-0005-0018.dcm', 'T1W_SE_2/IM-0005-0032.dcm',
'T1W_SE_1/IM-0004-0004.dcm', 'T1W_SE_2/IM-0005-0005.dcm', 'T1W_SE_2/IM-0005-0019.dcm', 'T1W_SE_2/IM-0005-0033.dcm',
'T1W_SE_1/IM-0004-0005.dcm', 'T1W_SE_2/IM-0005-0006.dcm', 'T1W_SE_2/IM-0005-0020.dcm', 'T1W_SE_2/IM-0005-0034.dcm',
'T1W_SE_1/IM-0004-0006.dcm', 'T1W_SE_2/IM-0005-0007.dcm', 'T1W_SE_2/IM-0005-0021.dcm', 'T1W_SE_2/IM-0005-0035.dcm',
'T1W_SE_1/IM-0004-0007.dcm', 'T1W_SE_2/IM-0005-0008.dcm', 'T1W_SE_2/IM-0005-0022.dcm', 'T1W_SE/IM-0003-0014.dcm',
'T1W_SE_1/IM-0004-0008.dcm', 'T1W_SE_2/IM-0005-0009.dcm', 'T1W_SE_2/IM-0005-0023.dcm', 'T1W_SE/IM-0003-0015.dcm',
'T1W_SE_1/IM-0004-0009.dcm', 'T1W_SE_2/IM-0005-0010.dcm', 'T1W_SE_2/IM-0005-0024.dcm', 'T1W_SE/IM-0003-0016.dcm',
'T1W_SE_1/IM-0004-0010.dcm', 'T1W_SE_2/IM-0005-0011.dcm', 'T1W_SE_2/IM-0005-0025.dcm', 'T1W_SE/IM-0003-0017.dcm',
'T1W_SE_1/IM-0004-0011.dcm', 'T1W_SE_2/IM-0005-0012.dcm', 'T1W_SE_2/IM-0005-0026.dcm', 'T1W_SE/IM-0003-0018.dcm',
'T1W_SE_1/IM-0004-0012.dcm', 'T1W_SE_2/IM-0005-0013.dcm', 'T1W_SE_2/IM-0005-0027.dcm', 'T1W_SE/IM-0003-0019.dcm',
'T1W_SE_1/IM-0004-0013.dcm', 'T1W_SE_2/IM-0005-0014.dcm', 'T1W_SE_2/IM-0005-0028.dcm', 'T1W_SE/IM-0003-0020.dcm',
'T1W_SE_2/IM-0005-0001.dcm', 'T1W_SE_2/IM-0005-0015.dcm', 'T1W_SE_2/IM-0005-0029.dcm',
];

class DateFrame extends Component {
  render() {
    return (
      <div className="frame">
        <span className={'button ' + reloadClassName(this.props.reloading)}
            onClick={this.props.onReloadDate}>
          Reload date
        </span>
        <span className={'date-display ' + reloadErrorClassName(this.props.reloading, this.props.error)}>
          {this.props.dateString}
        </span>
      </div>
    );
  }
}

class PatientFrame extends Component {
  render() {
    return (
      <div className="frame">
        <span
            className={'button ' + reloadClassName(this.props.reloading)}
            onClick={this.props.onReloadPatientName}>
          Reload name
        </span>
        <span className={'patient-display ' + reloadErrorClassName(this.props.reloading, this.props.error)}>
          {this.props.patientNameString}
        </span>
      </div>
    );
  }
}

class App extends Component {
  constructor() {
    super();
    this.state = {
      dateString: 'UNKNOWN DATE',
      dateReloading: false,
      dateError: false,
      patientNameString: 'UNKNOWN PATIENT',
      patientReloading: false,
      patientError: false,
      dicomNumber: 0,
    };
  }

  reloadDate() {
    if (this.state.dateReloading) {
      return;
    }
    this.setState({dateReloading: true});
    fetch(`${apiURL}/date`)
      .then(resp => (resp.json()))
      .then(data => {
        this.setState({dateReloading: false, dateError: false, dateString: data.date})
      })
      .catch(error => {
        console.log(error);
        this.setState({dateError: true, dateReloading: false});
      });
  }

  reloadPatientName() {
    if (this.state.patientReloading) {
      return;
    }
    this.setState({patientReloading: true});
    fetch(`${apiURL}/dicom?filename=${allDicoms[this.state.dicomNumber]}`)
      .then(resp => (resp.json()))
      .then(data => {
        this.setState({
          patientReloading: false,
          patientError: false,
          patientNameString: `Patient: ${data.patient_name} File: ${data.file_name}`
        });
      })
      .catch(error => {
        console.log(error);
        this.setState({patientError: true, patientReloading: false});
      });

    let nextDicom = this.state.dicomNumber + 1;
    if (nextDicom >= allDicoms.length) {
      nextDicom = 0;
    }
    this.setState({dicomNumber: nextDicom})
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">QMENTA devtest FE</h1>
        </header>
        <DateFrame
          dateString={this.state.dateString}
          reloading={this.state.dateReloading}
          error={this.state.dateError}
          onReloadDate={this.reloadDate.bind(this)}
        />
        <PatientFrame
          patientNameString={this.state.patientNameString}
          reloading={this.state.patientReloading}
          error={this.state.patientError}
          onReloadPatientName={this.reloadPatientName.bind(this)}
        />
      </div>
    );
  }
}

export default App;
