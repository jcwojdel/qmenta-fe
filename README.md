# Qmenta devtest frontend

This frontend is really a minimal solution to the challenge, but it basically works.
Note the following decisions that deviate from the literal challenge text:

- It uses React, because... why not? I decided it would feel strange to use plain JS, or jQuery, or something.
- It contains silly hardcoded list of dicoms available on the server, so that by clicking on the
  reload button you can loop over all of them.
  
Things I am not too proud of:

- All the components are simply dropped inside `App.js`, but there are not
  that many of them, really.
- Some code regarding button and label behaviour could be shared instead of duplicated.
- There are no tests. Really, that should not happen, but my heart is in backend.

## Live demo
Final result can be seen in [Heroku Frontend App](http://qmenta-fe.herokuapp.com/) (it may load slowly first time 
after the worker went to sleep).

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
